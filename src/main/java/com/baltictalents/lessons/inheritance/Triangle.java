package com.baltictalents.lessons.inheritance;

public class Triangle extends Figure {

    private final Double a;
    private final Double b;
    private final Double c;

    public Triangle(Double a, Double b, Double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public Integer edgesCount() {
        return 3;
    }

    public Double area() {
        Double halfP = perimeter() / 2;
        return Math.sqrt(halfP * (halfP - a) * (halfP - b) * (halfP - c));
    }

    public Double perimeter() {
        return a + b + c;
    }
}
