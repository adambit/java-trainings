package com.baltictalents.lessons.inheritance;

public abstract class Figure {

    public Boolean hasEdges() {
        return edgesCount() > 0;
    }

    protected abstract Integer edgesCount();
    public abstract Double area();
    public abstract Double perimeter();
}
