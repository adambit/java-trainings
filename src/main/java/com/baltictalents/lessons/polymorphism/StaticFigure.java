package com.baltictalents.lessons.polymorphism;

public abstract class StaticFigure implements Figure {

    private Double area;
    private Double perimeter;

    public StaticFigure(Double area, Double perimeter) {
        this.area = area;
        this.perimeter = perimeter;
    }
}
