package com.baltictalents.lessons.work.realestate;

import java.math.BigDecimal;

public class Owner {

    private final String firstname;
    private final String lastname;
    private final String phone;

    public Owner(String firstname, String lastname, String phone) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.phone = phone;
    }
}
