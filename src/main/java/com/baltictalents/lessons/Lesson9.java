package com.baltictalents.lessons;

import com.baltictalents.Main;

import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

public class Lesson9 {

    // multithreading

    List<Integer> list = new ArrayList<>();

    void lessonWork() {
        Executor executor = Executors.newFixedThreadPool(5);

        Lesson9 lesson9 = new Lesson9();

        for (int i = 1; i < 5; i++) {
            executor.execute(() -> {
                IntStream.range(1, 101)
                        .forEach(lesson9::printer);
            });
        }
    }

    // how to aggregate results from async task/futures
    void futures() throws Exception {
        ExecutorService executor = Executors.newFixedThreadPool(5);

        Long start = System.currentTimeMillis();

        Future<Integer> futureA = executor.submit(() -> {
            Thread.sleep(1000);
            return 100;
        });

        Future<Integer> futureB = executor.submit(() -> {
            Thread.sleep(500);
            return 200;
        });

        System.out.println(futureA.get() + futureB.get());
        System.out.println("Time: " + (System.currentTimeMillis() - start));
    }

    void printer(Integer i) {
        synchronized (list) {
            if (!list.contains(i)) {
                list.add(i);
            }
        }

        System.out.println(i);
    }

    void sortMap() {
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put("1", 2);
        map.put("2", 1);
        map.put("3", 4);
        map.put("4", 4);
        map.put("5", -1);
        map.put("6", 4);

        List<Map.Entry<String, Integer>> sortedList = new ArrayList<>(map.entrySet());
        Collections.sort(sortedList, (o1, o2) -> o1.getValue() - o2.getValue());

        for (Map.Entry<String, Integer> e: sortedList) {
            System.out.println(e.getKey() + " " + e.getValue());
        }
    }
}
