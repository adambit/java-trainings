package com.baltictalents.tasks;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class Tasks7 {

    // testuojantis naudoti POSTMAN

    // sukurti http servisa, kuris turetu du endpointus:
    // 1. GET http://localhost:8080/currencies
    // 2. GET http://localhost:8080/currency/rates?currencyCode=USD
    // responsai turi buti grazinami json formatu.
    //  pvz: ["USD", "EUR", "GBP"]
    void task1() {

    }

    // valiutu kursus gauti is vieso API
    // kad per daznai nenaudoti vieso API del greitaveikos,
    // valiutu kursus teks laikinai saugoti atmintyje valanda laiko.
    void task2() {

    }


    void howToStartHttpServer() throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
        server.createContext("/currencies", new MyHandler());
        server.createContext("/currency/rates", new MyHandler());
        server.setExecutor(null); // creates a default executor
        server.start();
    }

    // http routes handler
    static class MyHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "[\"USD\", \"EUR\", \"GBP\"]";
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }
    static class CurrencyRateHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "[\"USD\", \"EUR\", \"GBP\"]";
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }
}
