package com.baltictalents;

import com.baltictalents.lessons.db.MysqlConnector;
import com.baltictalents.lessons.db.model.Comment;
import com.baltictalents.lessons.db.model.User;
import com.baltictalents.lessons.db.repository.CommentsRepository;
import com.baltictalents.lessons.db.repository.UsersRepository;
import com.baltictalents.lessons.service.CommentsService;
import com.baltictalents.lessons.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.*;
import java.net.InetSocketAddress;
import java.util.Optional;

public class Main {

    public static void main(String[] args) throws Exception {

    }

    void startAndBind() throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
        server.createContext("/comment", new CommentHandler(initObjectMapper(), new CommentsService(new CommentsRepository(new MysqlConnector().getConnection()))));
        //server.createContext("/currency/rates", new MyHandler());
        server.setExecutor(null); // creates a default executor
        server.start();
    }

    // http routes handler
    class CommentHandler implements HttpHandler {

        private ObjectMapper objectMapper;
        private CommentsService commentsService;

        public CommentHandler(ObjectMapper objectMapper, CommentsService commentsService) {
            this.objectMapper = objectMapper;
            this.commentsService = commentsService;
        }

        @Override
        public void handle(HttpExchange t) throws IOException {

            String response = null;
            Integer responseStatus = 200;
            switch (t.getRequestMethod()) {
                case "POST":
                    String request = readInputStream(t.getRequestBody());

                    try {
                        Comment comment = objectMapper.readValue(request, Comment.class);
                        commentsService.save(comment);
                        response = "";
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                        responseStatus = 500;
                    }

                    break;
                case "GET":
                    String q = t.getRequestURI().getQuery();
                    String[] params = q.split("=");

                    try {
                        if (params[0].equals("id")) {
                            Integer commentId = Integer.valueOf(params[1]);
                            Optional<Comment> commentOptional = commentsService.get(commentId);

                            if (commentOptional.isPresent()) {
                                response = objectMapper.writeValueAsString(commentOptional.get());
                            } else {
                                responseStatus = 404;
                                response = "";
                            }

                        } else {
                            responseStatus = 400;
                            response = "";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        response = "";
                        responseStatus = 500;
                    }
                    break;
                default:
                    response = "unsuported";
                    responseStatus = 400;
                    break;
            }

            t.sendResponseHeaders(responseStatus, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }

        private String readInputStream(InputStream inputStream) throws IOException {
            final int bufferSize = 1024;
            final char[] buffer = new char[bufferSize];
            final StringBuilder out = new StringBuilder();
            Reader in = new InputStreamReader(inputStream, "UTF-8");
            for (; ; ) {
                int rsz = in.read(buffer, 0, buffer.length);
                if (rsz < 0)
                    break;
                out.append(buffer, 0, rsz);
            }
            return out.toString();
        }
    }


    class CurrencyRateHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "[\"USD\", \"EUR\", \"GBP\"]";
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    ObjectMapper initObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return objectMapper;
    }

    static void useService() throws Exception {
        MysqlConnector mysqlConnector = new MysqlConnector();

        UsersRepository usersRepository =
                new UsersRepository(mysqlConnector.getConnection());

        CommentsRepository commentsRepository =
                new CommentsRepository(mysqlConnector.getConnection());


        UserService userService = new UserService(usersRepository, commentsRepository);

        User user = new User(8, "u11", "borat@email");

        userService.saveOrUpdate(user);

        userService.getUserCommentsCount().forEach(System.out::println);
    }

    static void useRepositories() throws Exception {
        MysqlConnector mysqlConnector = new MysqlConnector();

        CommentsRepository commentsRepository =
                new CommentsRepository(mysqlConnector.getConnection());

        UsersRepository usersRepository =
                new UsersRepository(mysqlConnector.getConnection());

        usersRepository.save(new User(null, "adomas1", "adomas@gmail.com"));
    }
}
