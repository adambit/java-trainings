public class Paskaita2 {

    // ciklai, masyvai, klases - objektai, funkcijos - metodai.

    void forLoop() {

        Integer n = 10;
        for (int i = 0; i < n; i++) {
            n = n + i;
            System.out.println(i);
        }

    }

    void whileLoop() {

        Integer n = 10;
        Integer i = 0;

        Boolean matchCondition = true;

        System.out.println(i);
        while (matchCondition) {
            if (i == n) {
                matchCondition = false;
            }
            System.out.println(i);
            i++;
        }
    }

    void doWhileLoop() {

        Integer n = 0;
        Integer i = 0;

        do {
            System.out.println(i);
            i++;
        } while (i < n);
    }

    void intArray() {
        int n = 10;
        int[] ints = new int[n];

        for (int i = 0; i < n; i++) {
            ints[i] = i * i;
        }

        for (int i = 0; i < n; i++) {
            System.out.println(ints[i]);
        }
    }

    Double squareRoot1(double n) {
        if (n >= 0) {
            return Math.sqrt(n);
        } else {
            return null;
        }
    }

    Double squareRoot2(double n) {

        Boolean isAllowed = true;

        return (n >= 0) ? Math.sqrt(n) : null;
    }

    void matrix() {
        Integer n = 10;
        Integer[][] matrix = new Integer[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    matrix[i][j] = 1;
                } else {
                    matrix[i][j] = 0;
                }
            }
        }

        for (Integer[] line: matrix) {
            for(Integer elem: line) {
                System.out.print(elem + "\t");
            }
            System.out.println();
        }
    }
}
