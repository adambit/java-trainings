import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;

public class Zmogus {

    final String name;
    final String lastname;
    String personalCode;
    Float weightKg;
    Short heightCm;
    Boolean hasChildren;
    LocalDate bornDate;
    // F - female, M - male
    Character sex;
    String bloodType;
    String[] transactions;

    final static String[] defaultBloodTypes = {"0", "B-", "AB+", "E"};

    private final static Double cmInM = 100.0;

    Zmogus(
            String name,
            String lastname,
            String personalCode,
            Float weightKg,
            Short heightCm,
            Boolean hasChildren,
            LocalDate bornDate,
            Character sex,
            String bloodType
    ) {
        this.name = name;
        this.lastname = lastname;
        this.personalCode = personalCode;
        this.weightKg = weightKg;
        this.heightCm = heightCm;
        this.hasChildren = hasChildren;
        this.bornDate = bornDate;
        this.sex = sex;
        this.bloodType = bloodType;
    }

    Double kmi() {
        Double heightM = heightCm / cmInM;
        System.out.println(heightM);

        Double kmi = weightKg / (heightM * heightM);
        BigDecimal bd = new BigDecimal(kmi);
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    static String[] defaultBloodTypes() {
        return defaultBloodTypes;
    }

    Integer age() {
        return LocalDate.now().getYear() - bornDate.getYear();
    }

    String initials() {
        return name.substring(0, 1) + lastname.substring(0, 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Zmogus zmogus = (Zmogus) o;
        return Objects.equals(name, zmogus.name) &&
                Objects.equals(lastname, zmogus.lastname) &&
                Objects.equals(personalCode, zmogus.personalCode) &&
                Objects.equals(weightKg, zmogus.weightKg) &&
                Objects.equals(heightCm, zmogus.heightCm) &&
                Objects.equals(hasChildren, zmogus.hasChildren) &&
                Objects.equals(bornDate, zmogus.bornDate) &&
                Objects.equals(sex, zmogus.sex) &&
                Objects.equals(bloodType, zmogus.bloodType) &&
                Arrays.equals(transactions, zmogus.transactions);
    }

    @Override
    public int hashCode() {

        int result = Objects.hash(name, lastname, personalCode, weightKg, heightCm, hasChildren, bornDate, sex, bloodType);
        result = 31 * result + Arrays.hashCode(transactions);
        return result;
    }
}
