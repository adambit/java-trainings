import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class Lesson4 {

    // objektu konstruktoriai, switch salyga, failo nuskaitymas ir irasymas i faila

    static Integer[] readNumbers(String filename) throws Exception {
        File file = new File("resources/" + filename);
        Scanner in = new Scanner(file);

        Integer numbersSize = in.nextInt();
        Integer[] numbers = new Integer[numbersSize];

        for (int i = 0; i < numbersSize; i++) {
            numbers[i] = in.nextInt();
        }
        in.close();
        return numbers;
    }

    static void writeNumbers(Integer[] numbers, String filename) throws Exception {
        PrintWriter writer = new PrintWriter("resources/" + filename, "UTF-8");
        for (Integer number: numbers) {
            writer.write(number + " ");
        }
        writer.close();
    }

    void switchCase() {
        String inputType = "Text"; // Text or Digit or Zero

        Point point = new Point();
        switch (inputType) {
            case "Text":
                point = new Point("1.0", "1.0");
                break;
            case "Digit":
                point = new Point(2.0, 2.0);
                break;
            case "Zero":
                point = new Point();
                break;
        }

    }
}
